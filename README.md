# WP + Lando

```sh
lando start && lando db-import .local/db/dumps/2020-11-15.gz

lando mysql 
msql -> SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))
```

login

- `root:toor`

Url:

- http://test-wp.lndo.site/
