<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '7235f2e99e6bcab1e2e3af9445b1727a8470bef2',
    'name' => 'johnpbloch/wordpress',
  ),
  'versions' => 
  array (
    'johnpbloch/wordpress' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '7235f2e99e6bcab1e2e3af9445b1727a8470bef2',
    ),
    'johnpbloch/wordpress-core' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '345c507a929aa1cdc82cff522dde3093c9f637fb',
    ),
    'johnpbloch/wordpress-core-installer' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '237faae9a60a4a2e1d45dce1a5836ffa616de63e',
    ),
    'wordpress/core-implementation' => 
    array (
      'provided' => 
      array (
        0 => 'dev-master',
      ),
    ),
  ),
);
